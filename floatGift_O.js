$(function(){
    var qtyGift = document.getElementById('demo').getAttribute('data-value'); //ALWAYS FILL ON FOOTER
    var FloatGift = document.getElementsByClassName('float-cintillo');

    if (FloatGift){
            console.log('Exist FloatGift');
        }
    else{
        console.log('Not Exist FloatGift');
    }
        $('.minicart__value strong').bind("DOMSubtreeModified",function(){
            var total_cart = ""
            total_cart = $(this).text();
            //total_cart = total_cart.toFixed(2);

            

            //console.log(total_cart);
            vtexjs.checkout.getOrderForm().done(function(orderForm) {
                var str = orderForm.value;
                str = str.toString();
                //console.log(orderForm.value);
                str = str.slice(0, -2);
                str = parseFloat(str).toFixed(2);
                if (qtyGift) {
                    amount = parseFloat(qtyGift).toFixed(2);
                }
                else{
                    amount = parseFloat(2499).toFixed(2);
                }
                //amount = parseFloat(2799).toFixed(2);
                //const amountWon = input => Math.subtract(input,2499);
                var amountWon = amount - str;
                if ( !amountWon ){
                	
                	if ($('.float-cintillo').hasClass('notAvailable')){
                        $('.float-cintillo').removeClass('notAvailable');
                    }
                	$('.float-cintillo p').html('Agrega $'+ amount.replace(".", ",") +' MXN y obtén un regalo con compra.');
                }
                else if ( amountWon  < 0){
                	if ($('.float-cintillo').hasClass('notAvailable')){
                        $('.float-cintillo').removeClass('notAvailable');
                    }
    				$('.float-cintillo p').html('Felicidades, ganaste un regalo!');
                }
                else{
                	if ($('.float-cintillo').hasClass('notAvailable')){
                        $('.float-cintillo').removeClass('notAvailable');
                    }
    				$('.float-cintillo p').html('Te faltan $'+ amountWon.toFixed(2).replace(".", ",") +' MXN para un regalo con compra');
                }
                                
            });

        });
        if ($('#mainSlider').length){
            $('#mainSlider').slick({
                arrows: true,
                prevArrow: "<button data-role='none' class='slick-arrow slick-prev'><svg class='svg-icon-arrow-left'><use xlink:href='#svg-icon-arrow-left'></use></svg></button>",
                nextArrow: "<button data-role='none' class='slick-arrow slick-next'><svg class='svg-icon-arrow-right'><use xlink:href='#svg-icon-arrow-right'></use></svg></button>",
                autoplay: true,
                autoplaySpeed: 5000,
                dots: !0
            });
            jq2(window).on('resize', function() {
                $('#mainSlider').slick('resize');
            });
        }
});