var countDown = setInterval(function () {makeTimer();}, 1000);
var textFinishCount = $("#myAwaitingText").text();

function makeTimer() {
    if ($("#timer")) {
        var myDate = document.getElementById("myDate").textContent;
        //console.log(myDate);
        //    var endTime = new Date("29 April 2018 9:56:00 GMT 01:00");
        var endTime = new Date(myDate);
        endTime = Date.parse(endTime) / 1000;

        var now = new Date();
        now = Date.parse(now) / 1000;

        var timeLeft = endTime - now;

        var days = Math.floor(timeLeft / 86400);
        var new_hours = 24 * days + Math.floor((timeLeft - days * 86400) / 3600);
        var hours = Math.floor((timeLeft - days * 86400) / 3600);
        var minutes = Math.floor((timeLeft - days * 86400 - hours * 3600) / 60);
        var seconds = Math.floor(timeLeft - days * 86400 - hours * 3600 - minutes * 60);

        var textDays;
        var textHours;
        var textMinutes;

        if (days == "01") {
            textDays = "Dia"
        }
        else{
            textDays = "Dias"
        }
        if (hours == "01") {
            textHours = "Hora"
        }
        else{
            textHours = "Horas"
        }
        if (minutes == "01") {
            textMinutes = "Minuto"
        }
        else{
            textMinutes = "Minutos"
        }

        if (hours < "10") {
            hours = "0" + hours;
        }
        if (minutes < "10") {
            minutes = "0" + minutes;
        }
        if (seconds < "10") {
            seconds = "0" + seconds;
        }

        //$("#days").html(days + "<span>" + textDays + "</span>");
        /*$("#hours").html(new_hours + "<span>" + textHours + "</span>");
        $("#minutes").html(minutes + "<span>" + textMinutes + "</span>");
        $("#seconds").html(seconds + "<span>Segundos</span>");*/
        $("#hours").html(new_hours );
        $("#minutes").html(minutes );
        $("#seconds").html(seconds );

        if ( days < 0) {
            clearInterval(countDown); //stop timer
            $("#timer").html("<div class='textFinishCount'>" + textFinishCount + "</div>");
        }
    } else {
        console.log("no timer found");
    }
}
