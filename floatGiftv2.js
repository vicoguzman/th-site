$(function () {
    var isCustom = document.getElementById('customText').getAttribute('data-value'); //ALWAYS FILL ON RAPSODIAFLOATGIFT
    var qtyGift = document.getElementById('qtyAmountGift').getAttribute('data-value'); //ALWAYS FILL ON RAPSODIAFLOATGIFT
    var initialTextGift = document.getElementById('initialTextGift').getAttribute('data-value'); //ALWAYS FILL ON RAPSODIAFLOATGIFT
    var initialSecondTextGift = document.getElementById('initialSecondTextGift').getAttribute('data-value'); //ALWAYS FILL ON RAPSODIAFLOATGIFT
    var textAfterAdd = document.getElementById('textAfterAdd').getAttribute('data-value'); //ALWAYS FILL ON RAPSODIAFLOATGIFT
    var firstTextAfterAdd = document.getElementById('firstTextAfterAdd').getAttribute('data-value'); //ALWAYS FILL ON RAPSODIAFLOATGIFT
    var textGift = document.getElementById('textGift').getAttribute('data-value'); //ALWAYS FILL ON RAPSODIAFLOATGIFT
    var urlTarget = document.getElementById('urlTarget').getAttribute('data-value'); //ALWAYS FILL ON RAPSODIAFLOATGIFT
    var FloatGift = document.getElementsByClassName('float-cintillo');

    if (FloatGift) {
        console.log('Exist FloatGift');
    }
    else {
        console.log('Not Exist FloatGift');
    }
    $('.minicart__value strong').bind("DOMSubtreeModified", function () {
        var total_cart = ""
        total_cart = $(this).text();
        //total_cart = total_cart.toFixed(2);



        //console.log(total_cart);
        vtexjs.checkout.getOrderForm().done(function (orderForm) {
            var str = orderForm.value;
            str = str.toString();
            //console.log(orderForm.value);
            str = str.slice(0, -2);
            str = parseInt(str);
            if (qtyGift) {
                amount = parseInt(qtyGift);
            }
            else {
                amount = parseInt(2499);
            }

            //console.log("After truncate: ",str);
            //const amountWon = input => Math.subtract(input,2499);
            const amountWon = amount - str;
            if (!amountWon) {

                if ($('.float-cintillo').hasClass('notAvailable')) {
                    $('.float-cintillo').removeClass('notAvailable');
                }
                $('.float-cintillo p').html(initialTextGift + ' $' + Intl.NumberFormat('en-US').format(qtyGift) + initialSecondTextGift);
            }
            else if (amountWon < 0) {

                if ($('.float-cintillo').hasClass('notAvailable')) {
                    $('.float-cintillo').removeClass('notAvailable');
                }
                if(isCustom == "yes"){
                    $('.float-cintillo p').html('<a href="'+ urlTarget +'">'+ textGift +'</a>');
                }
                else{
                    $('.float-cintillo p').html(textGift);
                }
                
            }
            else {
                if ($('.float-cintillo').hasClass('notAvailable')) {
                    $('.float-cintillo').removeClass('notAvailable');
                }

                $('.float-cintillo p').html(firstTextAfterAdd + Intl.NumberFormat('en-US').format(amountWon) + ' MXN' + textAfterAdd);
            }



        });

    });

});