var newSliderOptions = {
    adaptiveHeight: !0,
    autoplay: false,
    autoplaySpeed: 5000,
    arrows: false,
    prevArrow: "<button data-role='none' class='slick-arrow slick-prev'><svg class='svg-icon-arrow-left'><use xlink:href='#svg-icon-chevron-left'></use></svg></button>",
    nextArrow: "<button data-role='none' class='slick-arrow slick-next'><svg class='svg-icon-arrow-right'><use xlink:href='#svg-icon-chevron-right'></use></svg></button>",
    cssEase: "linear",
    centerMode: !1,
    centerPadding: "0px",
    dots: false,
    focusOnSelect: !1,
    infinite: !1,
    slidesToShow: 4,
    responsive: [
        {
            breakpoint: 1920,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 2
            }
        },
        {
            breakpoint: 360,
            settings: {
                slidesToShow: 1
            }
        }
    ],
    touchMove: !0,
    variableWidth: false
}
sliderCall('#newSliderCat', newSliderOptions);