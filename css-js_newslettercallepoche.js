$(function(e){
    $("#newsletterCallePoche").validate({
        debug: true,
        rules:{
            callepocheEmail:{required:true},
            callepocheBirth:{required:true},
        },
        messages:{
            callepocheBirth:{required: "Ingrese su fecha de nacimiento"},
            callepocheEmail:{required: "Email es requerido", email: "Debe ser un Email v&aacute;lido",}
        },
        submitHandler: function (form) {
			var datos      = {};
			datos.email    = $('#callepocheEmail').val();
			datos.birthday = $("#callepocheBirth").val();

			$.ajax({
		        accept: 'application/vnd.vtex.ds.v10+json',
				contentType: 'application/json; charset=utf-8',
				crossDomain: true,
				data: JSON.stringify(datos),
				type: 'POST',
				url: '/api/dataentities/NC/documents',
		        beforeSend: function(){
		        	$("#btnSendCallePoche").attr("disabled",true);
		        	$("#btnSendCallePoche").val("Enviando...");
		        },
		        success: function (data, textStatus, xhr) {
		        	$("#btnSendCallePoche").attr("disabled",false);
	                $("#btnSendCallePoche").val("Suscribirme");


	                $("#newsletterCallePoche")[0].reset();
	                $("#callepoche-form").hide();
	                $("#callepoche-success").slideDown();
	                //$("#sendSuccessN").html('<div class="alert success">Â¡Gracias! Te has registrado correctamente en el bolet&iacute;n.</div>')
	                //$("#sendSuccessN").show();
		        },
			    complete: function(xhr, textStatus) {
			    	$("#btnSendCallePoche").attr("disabled",false);
	                $("#btnSendCallePoche").val("Suscribirme");

			    	if(xhr.status != 201){
			    		$("#newsletterCallePoche")[0].reset();
			    		$("#sendErrorN").show();
			    	}
			    }
		    });
		    return false; // required to block normal submit ajax used
        }
    });
});