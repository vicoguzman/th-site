if ($('#mainSlider').length){
	$('#mainSlider').slick({
		arrows: true,
		prevArrow: "<button data-role='none' class='slick-arrow slick-prev'><svg class='svg-icon-arrow-left'><use xlink:href='#svg-icon-arrow-left'></use></svg></button>",
		nextArrow: "<button data-role='none' class='slick-arrow slick-next'><svg class='svg-icon-arrow-right'><use xlink:href='#svg-icon-arrow-right'></use></svg></button>",
		autoplay: true,
		autoplaySpeed: 5000,
		dots: !0
	});
	jq2(window).on('resize', function() {
		$('#mainSlider').slick('resize');
	});
}